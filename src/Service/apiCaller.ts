import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const GET_AXIOS = async (endpoint: string, body = {}) => {
  const instance = axios.create({
    baseURL: endpoint,
    timeout: 3000,
    // axios accept header
  });
  const token = await AsyncStorage.getItem('jwt');
  if (token) {
    instance.defaults.headers.Authorization = 'Bearer ' + token;
  }
  instance.defaults.headers['Content-Type'] = 'application/json';
  instance.defaults.headers['Accept'] = 'application/vnd.github.v3+json';
  return instance.get('', body);
};
