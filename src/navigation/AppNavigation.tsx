import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {SafeAreaProvider} from 'react-native-safe-area-context';

import {AppearanceProvider} from 'react-native-appearance';
import HomeStackNavigator from './HomeStack/HomeStackNavigator';

export type RootStackParams = {
  HomeStackNavigator: undefined;
};

const Stack = createStackNavigator<RootStackParams>();

const AppNavigation = () => {
  return (
    <SafeAreaProvider>
      <AppearanceProvider>
        <NavigationContainer>
          <Stack.Navigator
            screenOptions={{
              headerShown: false,
            }}>
            <Stack.Screen
              name={'HomeStackNavigator'}
              component={HomeStackNavigator}
            />
          </Stack.Navigator>
        </NavigationContainer>
      </AppearanceProvider>
    </SafeAreaProvider>
  );
};

export default AppNavigation;
