import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
//import Screen
import HomeScreen from '../../containers/HomeScreen/HomeScreen';

export type HomeStackParams = {
  HomeScreen: undefined;
};

const HomeStack = createStackNavigator<HomeStackParams>();

const HomeStackNavigator = () => (
  <HomeStack.Navigator
    screenOptions={{
      headerShown: false,
    }}>
    <HomeStack.Screen name="HomeScreen" component={HomeScreen} />
  </HomeStack.Navigator>
);

export default HomeStackNavigator;
