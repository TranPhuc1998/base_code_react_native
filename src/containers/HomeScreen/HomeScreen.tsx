import React, {useState} from 'react';
import {SafeAreaView, StyleSheet, View, Text} from 'react-native';
// import Components
import SearchBar from '../../components/SearchBar/SearchBar';

// import themes
import {hScale} from '../../themes/Responsive';

//import component

const HomeScreen = () => {
  const [value, setValue] = useState<string>('');
  return (
    <SafeAreaView style={styles.home}>
      <View style={styles.topBox}>
        <Text style={styles.textTopBox}>GitHub</Text>
      </View>

      <View style={styles.viewSearch}>
        <SearchBar
          placeholder="Search..."
          style={{backgroundColor: 'red'}}
          value={value}
          onChangeText={e => setValue(e)}
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  home: {
    flex: 1,
  },
  textTopBox: {
    paddingTop: hScale(40),
    color: '#7FFF00',
    fontWeight: 'bold',
  },
  topBox: {
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },

  viewSearch: {
    paddingTop: 35,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default HomeScreen;
