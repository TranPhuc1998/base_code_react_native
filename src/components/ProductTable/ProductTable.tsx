import React from 'react';
import {View} from 'react-native';
import ProductCategoryItem from '../ProductCategoryItem/ProductCategoryItem';
import ProductItem from '../ProductItem/ProductItem';

interface Props {
  products: Array<any>;
}

const ProductTable: React.FC<Props> = ({products}) => {
  return (
    <View>
      <ProductCategoryItem />
      <ProductItem />
    </View>
  );
};

export default ProductTable;
