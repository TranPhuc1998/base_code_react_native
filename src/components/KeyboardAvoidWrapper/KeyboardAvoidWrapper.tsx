import React from 'react';
import {KeyboardAvoidingView, ScrollView, Platform} from 'react-native';

interface IKeyboardAvoidingView {
  children?: any;
}

const KeyboardAvoidWrapper: React.FC<IKeyboardAvoidingView> = ({children}) => {
  const behavior = Platform.OS === 'ios' ? 'padding' : undefined;
  return (
    <KeyboardAvoidingView behavior={behavior}>
      <ScrollView bounces={false} contentContainerStyle={{flexGrow: 1}}>
        {children}
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

export default KeyboardAvoidWrapper;
