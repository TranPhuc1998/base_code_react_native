import React from 'react';
import {View, StyleSheet, StyleProp, ViewStyle, TextInput} from 'react-native';
import {} from 'react-native-gesture-handler';
import {hScale} from '../../themes/Responsive';
import KeyboardAvoidWrapper from '../KeyboardAvoidWrapper/KeyboardAvoidWrapper';

interface ISearchBar {
  placeholder?: string;
  value?: string;
  onChangeText?: (text: string) => void;
  style?: StyleProp<ViewStyle>;
}
const SearchBar: React.FC<ISearchBar> = ({
  placeholder,
  value,
  onChangeText,
  style,
}) => {
  return (
    <KeyboardAvoidWrapper>
      <View style={[styles.searchBar, style]}>
        <TextInput
          placeholder={placeholder}
          value={value}
          onChangeText={onChangeText}
        />
      </View>
    </KeyboardAvoidWrapper>
  );
};

const styles = StyleSheet.create({
  searchBar: {
    borderRadius: 45,
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#7FFF00',
    borderWidth: 10,
    borderColor: '#fff',
    height: hScale(70),
    borderBottomWidth: 2,
  },
});

export default SearchBar;
