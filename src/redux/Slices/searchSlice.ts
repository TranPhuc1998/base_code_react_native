import {createSlice, PayloadAction} from '@reduxjs/toolkit';

export type Search = Array<{}>;

const initialState = [] as Search;

const search = createSlice({
  name: 'search',
  initialState,
  reducers: {
    SEARCH_INFO: (state, action: PayloadAction<t>) => {},
  },
});

const {reducer, actions} = search;
export const {SEARCH_INFO} = actions;
export default reducer;
