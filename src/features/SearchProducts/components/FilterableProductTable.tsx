import React, {useEffect, useState} from 'react';
import {View} from 'react-native';
import ProductTable from '../../../components/ProductTable/ProductTable';
import SearchBar from '../../../components/SearchBar/SearchBar';

import mockProducts from '../mocks/products';

function fakeAPIRequest(ms: number) {
  const promise = new Promise(function (resolve) {
    setTimeout(() => {
      resolve(mockProducts);
    }, ms);
  });

  return promise;
}

const FilterableProductTable = () => {
  const [products, setProducts] = useState<Array<any>>([]);

  useEffect(() => {
    fakeAPIRequest(1500).then(function (resProducts) {
      setProducts(resProducts as Array<any>);
    });
  }, []);

  return (
    <View>
      <SearchBar />
      <ProductTable products={products} />
    </View>
  );
};

export default FilterableProductTable;
