import React from 'react';
import {View} from 'react-native';

import FilterableProductTable from './components/FilterableProductTable';

const SearchProductsScreen = () => {
  return (
    <View>
      <FilterableProductTable />
    </View>
  );
};

export default SearchProductsScreen;
